from geopy import distance
from io import StringIO
import pandas as pd
import psycopg2
import requests

import instance.config

maps_endpoint = 'https://maps.googleapis.com/maps/api/geocode/json'
google_api_key = instance.config.GOOGLE_API_KEY


# PostgreSQL to pandas dataframe method from
# naysan.ca/2020/05/31/postgresql-to-pandas
def get_weather_stations():
    db = psycopg2.connect(instance.config.CONNSTRING)
    cur = db.cursor()

    column_names = ['noaa_id', 'lat', 'long']
    sql = 'SELECT '
    for name in column_names:
        sql += name + ', '
    sql = sql[:-2] + ' FROM climate_data;'
    cur.execute(sql)
    data = cur.fetchall()
    cur.close()
    db.close()

    df = pd.DataFrame(data, columns=column_names)
    return df


def get_zip_codes():
    db = db = psycopg2.connect(instance.config.CONNSTRING)
    cur = db.cursor()

    sql = "SELECT DISTINCT zcta5 FROM population_data where state = '12'"
    cur.execute(sql)
    data = cur.fetchall()
    cur.close()
    db.close()

    df = pd.DataFrame(data, columns=['zip'])
    return df


def get_distance(location, lat, long):
    return distance.distance(location, (lat, long)).miles


def find_nearest(df, zip_code):
    print(zip_code)
    query = {'components': 'postal_code:' + zip_code, 'key': google_api_key}
    response = requests.get(maps_endpoint, params=query).json()
    location = response['results'][0]['geometry']['location']
    near_to = (location['lat'], location['lng'])

    df['lat'] = df['lat'].astype(float)
    df['long'] = df['long'].astype(float)
    df['far'] = df.apply(lambda x: distance.distance(
        near_to, (x['lat'], x['long'])).miles, axis=1)
    min_row = df['far'].idxmin()
    noaa_id = df.iloc[min_row]['noaa_id']

    return noaa_id


def write_df(df):
    db = db = psycopg2.connect(instance.config.CONNSTRING)
    cur = db.cursor()

    buffer = StringIO()
    df.to_csv(buffer, index=False, index_label=False, header=False)
    buffer.seek(0)

    query = 'CREATE TABLE zip_lookup (zip VARCHAR, weather_station VARCHAR);'
    cur.execute(query)
    db.commit()

    try:
        cur.copy_from(buffer, 'zip_lookup', sep=',')
        db.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print('Error: %s' % error)
        db.rollback()
        cur.close()
        return 1

    cur.close()


df = get_weather_stations()
zip_lookup = get_zip_codes()
zip_lookup['weather_station'] = zip_lookup.apply(
    lambda x: find_nearest(df, x['zip']), axis=1)
print(zip_lookup)
write_df(zip_lookup)
