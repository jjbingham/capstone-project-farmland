# Project Farmland
This project is intended to help potential farmers identify farm properties that
meet their particular farm needs.

It currently reports average annual precipitaion, length of the growing season,
population density, and topography variability for addresses in Florida. Population
density and topography variability are available for addresses in the United States.

## Important Notes
There is an instande/config.py file with secure keys that is not maintained in the git repo
for security. This file and folder will be necessary to run the repo.

An example config file is provided: instance.config.py

These instructions are for Mac/Linux only.

## Installation (These have not yet been tested)
Install PostgreSQL and create database `farmland`

Create the virtual env: `python3 -m venv .venv`

Start the virtual env: `source .venv/bin/activate`

Install requirements:  `pip install -r requirements.txt`

Give the PostgreSQL user (postgres) permissions on the data directory: 
`sudo chown postgres flaskr/data`

Initialize the db: `flask init-db` (Note: this is not yet fully implemented)

## Start the App
Activate the virtual environment: `source .venv\scripts\activate.bat`

Start the Flask app:
```
export FLASK_APP=flaskr
export FLASK_ENV=development
flask run
```
## Sample API Call
curl -X POST http://your.url.here:5000/get_characteristics
-H 'Content-Type: application/json'
-H 'Accept: application/json'
-d '{"street": "Street Address",
     "city": "City",
     "state": "State",
     "zip": "Zip Code"}'