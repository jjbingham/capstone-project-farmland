import os
import zipfile
# import pandas as pd
import psycopg2
import requests

import instance.config

# files = os.listdir('./sales_data')
# files.sort(reverse=True)

db = psycopg2.connect(instance.config.CONNSTRING)
cur = db.cursor()


def get_sales_data(file):
    with zipfile.ZipFile('./sales_data/' + file, 'r') as sales_zip:
        filename = sales_zip.namelist()[0]
        with sales_zip.open(filename) as sales_file:
            # Throw away first line (headers)
            sales_file.readline()

            # Get the parcel ids and sale prices from file
            while True:
                line = sales_file.readline()

                # Check for end of file
                if not line:
                    break

                values = line.decode("utf-8").split(',')
                parcel_id = values[1]
                sale_price = values[18]

                if int(sale_price) > 0:
                    sql = 'INSERT INTO sales_data(parcel_id, sale_price)'
                    sql += 'VALUES (%s, %s);'

                    cur.execute(sql, (parcel_id, sale_price))
                    # sales_data[parcel_id] = [parcel_id, sale_price]
                    db.commit()


def get_address_data(file):
    print(file)

    # Catch rows that throw a duplicate key error for further inspection
    duplicate_rows = []

    with zipfile.ZipFile('./sales_data/' + file, 'r') as address_zip:
        filename = address_zip.namelist()[0]
        with address_zip.open(filename) as address_file:
            # Throw away the first line (headers)
            address_file.readline()

            # Get the parcel ids and addresses from file
            while True:
                line = address_file.readline()

                # Check for end of file
                if not line:
                    break

                values = line.decode("utf-8").split(',')
                parcel_id = values[1]
                street = values[98]
                city = values[100]
                state = 'FL'
                zip = values[101]
                print(parcel_id, street, city, state, zip)

                sql = 'INSERT INTO address_data('
                sql += 'parcel_id, street, city, state, zip)'
                sql += 'VALUES(%s, %s, %s, %s, %s);'

                try:
                    cur.execute(sql, (parcel_id, street, city, state, zip))
                except psycopg2.errors.UniqueViolation:
                    duplicate_rows.append(
                        [parcel_id, street, city, state, zip])
                db.commit()
    print(duplicate_rows)


def create_sales_table():
    sql = 'CREATE TABLE sales_data ('
    sql += 'parcel_id VARCHAR, sale_price BIGINT);'

    cur.execute(sql)
    db.commit()


def create_address_table():
    sql = 'CREATE TABLE address_data ('
    sql += 'parcel_id VARCHAR PRIMARY KEY, '
    sql += 'street VARCHAR, '
    sql += 'city VARCHAR, '
    sql += 'state VARCHAR(2), '
    sql += 'zip VARCHAR);'

    cur.execute(sql)
    db.commit()


def create_final_sales_data_table():
    sql = 'CREATE TABLE final_sales_data ('
    sql += 'parcel_id VARCHAR,'
    sql += 'sale_price BIGINT,'
    sql += 'precip NUMERIC,'
    sql += 'growing_season_50 INT,'
    sql += 'topo_variability NUMERIC,'
    sql += 'population_density NUMERIC);'

    cur.execute(sql)
    db.commit()


def write_final_dataset_to_db():
    characteristics_endpoint = 'http://localhost:5000/get_characteristics'
    sql = 'SELECT sales_data.parcel_id, sales_data.sale_price, '
    sql += 'address_data.street, address_data.city, address_data.state, '
    sql += 'address_data.zip '
    sql += 'FROM sales_data JOIN address_data '
    sql += 'ON sales_data.parcel_id = address_data.parcel_id;'

    sql2 = 'INSERT INTO final_sales_data('
    sql2 += 'parcel_id, sale_price, precip, growing_season_50, '
    sql2 += 'topo_variability, population_density) '
    sql2 += 'VALUES(%s, %s, %s, %s, %s, %s);'

    cur.execute(sql)
    db2 = psycopg2.connect(instance.config.CONNSTRING)
    cur2 = db2.cursor()
    for record in cur:
        parcel_id = record[0]
        sales_price = record[1]
        street = record[2]
        city = record[3]
        state = record[4]
        zip_code = record[5]
        print(zip_code)

        header = {'Content-Type': 'application/json',
                  'Accept': 'application/json'}
        post_data = {'street': street,
                     'city': city,
                     'state': state,
                     'zip': zip_code}

        try:
            response = requests.post(characteristics_endpoint, json=post_data,
                                     headers=header).json()

            precip = response['climate_data']['precip']
            growing_season_50 = response['climate_data']['growing_season_50']
            topo_variability = response['topo_data']['elevation_sd']
            population_density = response['population_data']
        except Exception as e:
            print(e)
            precip = 0
            growing_season_50 = 0
            topo_variability = 0
            population_density = 0

        cur2.execute(sql2, (parcel_id, sales_price, precip, growing_season_50,
                            topo_variability, population_density))
        db2.commit()


# create_sales_table()
# create_address_table()
# for file in files:
#     if 'SDF' in file:
#         pass
#         # get_sales_data(file)
#     elif 'NAL' in file:
#         get_address_data(file)

# create_final_sales_data_table()
write_final_dataset_to_db()

cur.close()
db.close()
