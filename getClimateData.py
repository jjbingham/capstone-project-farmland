import requests
import json

import instance.config

# This script was used to investigate the NOAA api.
# The data I was looking for was more easily accessible by ftp
# None of the data obtained through this script was used in the final project
# It is included here in the interest of project completeness

NOAA_Token = instance.config.NOAA_API_KEY

header = {'token': NOAA_Token}
datasets_endpoint = 'https://www.ncdc.noaa.gov/cdo-web/api/v2/datasets'
datatypes_endpoint = 'https://www.ncdc.noaa.gov/cdo-web/api/v2/datatypes'
datacategories_endpoint = 'https://www.ncdc.noaa.gov/cdo-web/api/v2/datacategories'
location_categories_endpoint = 'https://www.ncdc.noaa.gov/cdo-web/api/v2/locationcategories'
locations_endpoint = 'https://www.ncdc.noaa.gov/cdo-web/api/v2/locations'
data_endpoint = 'https://www.ncdc.noaa.gov/cdo-web/api/v2/data'
# https://www.ncdc.noaa.gov/cdo-web/api/v2/data
# Precipitation, Instantaneous Precipitation Rate, Temperature mean
datatypes = 'HPCP'
# datatypes = 'HPCP&DPR&HLY-TEMP-NORMAL'
start_date = '2021-01-01'
end_date = '2021-12-31'


def get_datasets():
    query = {'limit': '1000'}
    response = requests.get(datasets_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))
    with open('all_datasets.json', 'w') as outfile:
        json.dump(response.json(), outfile, indent=4)


def get_data_categories():
    query = {'limit': '1000'}
    response = requests.get(datacategories_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))
    # with open('all_datacategories.json', 'w') as outfile:
    #     json.dump(response.json(), outfile, indent=4)


def get_dataypes():
    query = {'datasetid': 'GHCND', 'startdate': start_date, 'enddate': end_date}
    response = requests.get(datatypes_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))
    # with open('all_datatypes.json', 'w') as outfile:
    #     json.dump(response.json(), outfile, indent=4)


def get_location_categories():
    query = {'limit': '1000'}
    response = requests.get(location_categories_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))
    # with open('all_location_categories.json', 'w') as outfile:
    #     json.dump(response.json(), outfile, indent=4)


def get_locations():
    query = {'locationcategoryid': 'ZIP', 'limit': '1000'}
    response = requests.get(locations_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))
    with open('zip_locations.json', 'w') as outfile:
        json.dump(response.json(), outfile, indent=4)


def get_daily_summaries():
    # query = {'datasetid':'GHCND', 'datatypeid':'PRCP', 'locationid':'ZIP:33810', 'startdate': start_date, 'enddate': end_date, 'limit': '500'}
    query = {'datasetid': 'NORMAL_HLY', 'startdate': start_date, 'enddate': end_date, 'limit': '5'}
    response = requests.get(data_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))

    with open('prcip_summaries.json', 'w') as outfile:
        json.dump(response.json(), outfile, indent=4)


def get_test_data():
    # https://www.ncdc.noaa.gov/cdo-web/api/v2/data?datasetid=GHCND&locationid=ZIP:28801&startdate=2010-05-01&enddate=2010-05-01
    query = {'datasetid': 'GHCND', 'locationid': 'ZIP:33810', 'startdate': '2010-05-01', 'enddate': '2010-05-01'}
    response = requests.get(data_endpoint, headers=header, params=query)
    print(json.dumps(response.json(), indent=4))


get_dataypes()

# query = {'param1':'value1', 'param2':'value2'}
# response = requests.get ("endpoint", headers=header, params=query)
# print response.json
