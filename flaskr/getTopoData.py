import math
import numpy as np
from rasterio.io import MemoryFile
import requests
from io import BytesIO
import json

maps_endpoint = 'https://maps.googleapis.com/maps/api/geocode/json'
topo_endpoint = 'https://portal.opentopography.org/API/globaldem'

# TODO: Add OpenTopo license data
# TODO: Add GoogleMaps license data


def get_coords(address, google_api_key):
    address = address.replace(' ', '+')
    # query = {'address': address, 'key': instance.config.GOOGLE_API_KEY}
    query = {'address': address, 'key': google_api_key}
    response = requests.get(maps_endpoint, params=query).json()

    # This is where approximate property boundaries live in the Google Maps
    # response
    results = response['results'][0]['geometry']['bounds']
    print(json.dumps(results, indent=4))
    return results


def get_topo(bounds, topo_api_key):
    # Ensure a large enough area (must be at least 250m) by rounding to the
    # nearest 1/100. Currently only works in the north and west hemispheres
    north = math.ceil(float(bounds['northeast']['lat'])*100)/100
    east = math.ceil(float(bounds['northeast']['lng'])*100)/100
    south = math.floor(float(bounds['southwest']['lat'])*100)/100
    west = math.floor(float(bounds['southwest']['lng'])*100)/100
    query = {
        'demtype': 'SRTMGL1',
        'south': south,
        'north': north,
        'west': west,
        'east': east,
        'outputFormat': 'GTiff',
        'API_Key': topo_api_key
    }
    response = requests.get(topo_endpoint, params=query)
    if response.status_code in [400, 500]:
        print(response.text)

    data = BytesIO(response.content)
    return data


def calculate_variability(data):
    # Potential future improvement, limit calculation to JUST topography
    # inside the original bounding box

    elevation_sd = None

    # Calculate the standard deviation of the GeoTiff band 1 (elevation)
    with MemoryFile(data) as file:
        with file.open() as dataset:
            elevation_set = np.array(dataset.read(1))
            elevation_sd = np.std(elevation_set)

    return elevation_sd


def get_topo_variability(street, city, state, zip, google_key, topo_key):
    bounds = get_coords(
        street + ', ' + city + ', ' + state + ' ' + zip, google_key)
    data = get_topo(bounds, topo_key)

    return calculate_variability(data)

# bounds = get_coords('8625 Harrison Rd, Lakeland, FL 33810')
# data = get_topo(bounds)
# print('elevation sd', calculate_variability(data))
