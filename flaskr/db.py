from io import StringIO
import os
import pandas as pd
import psycopg2
import click

from flask import current_app, g
from flask.cli import with_appcontext

from . import importPopData
# from . import importClimate


def get_db():
    if 'db' not in g:
        g.db = psycopg2.connect(current_app.config['CONNSTRING'])

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_db():
    # db = get_db
    importPopData.import_population()

    # ONLY USE EXTERNAL SCRIPT IF /data/climate_data.csv is not available
    # importClimate.import_climate_data()
    import_climate_data()
    import_zip_lookup()


@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()


@click.command('export-data')
@with_appcontext
def export_data_command():
    export_table_to_csv('climate_data')
    export_table_to_csv('zip_lookup')


def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(export_data_command)


def write_dataframe(df, table_name):
    db = get_db()
    cur = db.cursor()

    # Drop table if exists
    query = 'DROP TABLE IF EXISTS ' + table_name + ';'
    cur.execute(query)
    db.commit()

    query = 'CREATE TABLE ' + table_name + ' ('

    print(df.dtypes)

    # Create table
    columns = df.columns
    for column in columns:
        dtype = df[column].dtype
        if dtype == 'int32':
            query += str(column) + ' INT, '
        elif dtype == 'int64':
            query += str(column) + ' BIGINT, '
        elif dtype == 'float64':
            query += str(column) + ' NUMERIC, '
        else:
            query += str(column) + ' VARCHAR, '

    query = query[:-2] + ');'

    cur.execute(query)
    db.commit()

    # Write to table
    # Based on naysan.ca/2020/05/09/
    # pandas-to-postgresql-using-psycopg2-bulk-insert-performance-benchmark/
    buffer = StringIO()
    df.to_csv(buffer, index=False, index_label=False, header=False)
    buffer.seek(0)

    try:
        cur.copy_from(buffer, table_name, sep=',')
        db.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print('Error: %s' % error)
        db.rollback()
        cur.close()
        return 1

    cur.close()


# Taken from
# https://www.geeksforgeeks.org/how-to-import-and-export-data-using-csv-files-in-postgresql/
def export_table_to_csv(table_name):
    full_path = os.path.realpath(__file__)
    dir_path = os.path.dirname(full_path)
    data_path = dir_path + '/data/' + table_name + '.csv'

    sql = "COPY " + table_name + " TO '" + data_path
    sql += "' DELIMITER ',' CSV HEADER;"
    print(sql)

    db = get_db()
    cur = db.cursor()
    cur.execute(sql)

    cur.close()


def import_climate_data():
    climate_data = pd.read_csv('flaskr/data/climate_data.csv')

    write_dataframe(climate_data, 'climate_data')


def import_zip_lookup():
    zip_lookup = pd.read_csv('flaskr/data/zip_lookup.csv')

    write_dataframe(zip_lookup, 'zip_lookup')


def get_climate_data(zip):
    db = get_db()
    cur = db.cursor()

    climate_data = None

    # TODO: Remove unnecessary zip_code data from climate_data table

    with current_app.open_resource('get_climate_data.sql') as f:
        print('sql', f.read().decode('utf8'), zip)
        f.seek(0)
        cur.execute(f.read().decode('utf8'), [zip])
        climate_data = cur.fetchone()

    cur.close()

    formatted_climate_data = {
        # 'NOAA_ID': climate_data[0],
        # 'lat': climate_data[1],
        # 'long': climate_data[2],
        # 'elev': climate_data[3],
        # 'zip': climate_data[4],
        'precip': climate_data[5],
        'growing_season_10': climate_data[6],
        'growing_season_50': climate_data[7],
        'growing_season_90': climate_data[8]
    }
    print('climate data', formatted_climate_data)

    return formatted_climate_data


def get_population_data(zip):
    db = get_db()
    cur = db.cursor()

    population_data = None
    sql = "SELECT popdensity FROM population_data "
    sql += "WHERE zcta5 = %s;"

    cur.execute(sql, [zip])
    population_data = cur.fetchone()

    return population_data[0]
