import geopy
from numpy import int32
import pandas as pd
import time

from . import db

geolocator = geopy.Nominatim(user_agent='capstone_project_farmland')

# Keep track of entries with no postcode
no_postcode = {}


# IMPORTANT NOTE: Display attribution for Nominatim (nominatim.org)
# Also governed by an Open Data Commons Open Database License
# (opendatacommons.org/licenses/odbl/)
def get_zipcode(df, geolocator, id_field, lat_field, lon_field):
    location = geolocator.reverse((df[lat_field], df[lon_field]))

    try:
        zipcode = location.raw['address']['postcode']
    except KeyError:
        no_postcode[df[id_field]] = location.raw['address']
        zipcode = ''

    time.sleep(1)

    return zipcode


def import_stations(state):
    no_postcode = []
    # Station file does not contain a header - header=1
    raw_stations = pd.read_fwf('climate_data/allstations.txt', header=1)

    # Add column headers based on
    # ftp://ftp.ncdc.noaa.gov/pub/data/normals/1081-2010/readme.txt
    raw_stations.columns = [
        'NOAA_ID',
        'Lat', 'Long',
        'Elev_km',
        'State',
        'Name',
        'Other1',
        'Other2',
        'Other3'
    ]

    # Filter to desired state (Nominatim has rate limits)
    station_data_fl = raw_stations[raw_stations['State'] == state]

    # Filter to desired columns
    station_data_fl = station_data_fl[['NOAA_ID', 'Lat', 'Long', 'Elev_km']]

    # Add zip code based on latitude and longitude using geopy
    # TODO: Uncomment
    station_data_fl['Zip_Code'] = station_data_fl.apply(
        get_zipcode, axis=1, geolocator=geolocator, id_field='NOAA_ID',
        lat_field='Lat', lon_field='Long')

    print("No postcode for the following:", no_postcode)
    return(station_data_fl)


def import_precipitation():
    precip = pd.read_fwf('climate_data/ann-prcp-normal.txt', header=1)

    # Add column headers based on
    # ftp://ftp.ncdc.noaa.gov/pub/data/normals/1081-2010/readme.txt
    precip.columns = ['NOAA_ID', 'Precip(1/100in)']

    # The last character indicates the completness of the underlying data set,
    # which I'm not going to worry about
    precip['Precip(1/100in)'] = precip['Precip(1/100in)'].str.replace(
        '[QPRSC]', '', regex=True)

    # Convert to a float and divide by 100 to get precipitation in inches
    precip['Precip(1/100in)'] = precip['Precip(1/100in)'].astype(
        float, errors='raise')/100
    precip.rename(columns={'Precip(1/100in)': 'Precip_in'}, inplace=True)

    return(precip)


def import_gs(filename, label):
    gs = pd.read_fwf('climate_data/'+filename, header=1)
    column_label = 'Growing_Season_days_' + label

    # Add column headers based on
    # ftp://ftp.ncdc.noaa.gov/pub/data/normals/1081-2010/readme.txt
    gs.columns = ['NOAA_ID', column_label]

    # The last character indicates the completness of the underlying data set,
    # which I'm not going to worry about
    gs[column_label] = gs[column_label].str.replace('[QPRSC]', '', regex=True)

    # Cast the number of days as an int for potential future math
    gs[column_label] = gs[column_label].astype(int32, errors='raise')
    print(gs.dtypes)
    return(gs)


def import_climate_data():
    # import and index all the data
    stations = import_stations('FL')
    stations.set_index(stations['NOAA_ID'])

    precip = import_precipitation()
    precip.set_index(precip['NOAA_ID'])

    gs10 = import_gs('ann-tmin-prbgsl-t32Fp10.txt', '10')
    gs10.set_index(gs10['NOAA_ID'])

    gs50 = import_gs('ann-tmin-prbgsl-t32Fp50.txt', '50')
    gs50.set_index(gs50['NOAA_ID'])

    gs90 = import_gs('ann-tmin-prbgsl-t32Fp90.txt', '90')
    gs90.set_index(gs90['NOAA_ID'])

    # merge the data into one dataframe
    climate_data = pd.concat(
        [stations, precip, gs10, gs50, gs90], axis=1, join='inner')
    climate_data = climate_data.loc[:, ~climate_data.columns.duplicated()]

    # write the data to the database
    db.write_dataframe(climate_data, 'climate_data')
