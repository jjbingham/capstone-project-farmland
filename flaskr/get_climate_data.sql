SELECT noaa_id, lat, long, elev_km, zip_code, precip_in,
growing_season_days_10, growing_season_days_50, growing_season_days_90
FROM climate_data JOIN zip_lookup 
ON climate_data.noaa_id = zip_lookup.weather_station
WHERE zip_lookup.zip = %s;