import pandas as pd

from . import db


def import_population():
    # Read in data as strings to preserve zip code as text
    raw_population = pd.read_csv(
        'population_data/zcta_county_rel_10.txt', dtype=str)

    # ZCTA5 = zip code, ZPOP = population, ZAREALAND = land area
    population_data = raw_population[['ZCTA5', 'ZPOP', 'ZAREALAND', 'STATE']]
    # Convert numeric types
    population_data['ZPOP'] = population_data['ZPOP'].astype(int)

    # For the purposes of this investigation, I'm using area of land since I
    # expect that this is a better metric than entire area, especially in
    # Florida where there are so many lakes. This results in a higher
    # population density than the one often reported online.
    population_data['ZAREALAND'] = population_data['ZAREALAND'].astype(int)

    # Caluclate population density and convert from people/square meter
    # to people/square mile
    population_data['PopDensity'] = (
        population_data['ZPOP']/(population_data['ZAREALAND']*0.00000038610))

    # Round decimals to a reasonable level of detail
    neat_population_data = population_data.round(6)

    db.write_dataframe(df=neat_population_data, table_name='population_data')
