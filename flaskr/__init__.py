import os

from flask import Flask
from flask import jsonify
from flask import request

from . import db
from . import getTopoData


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=False)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)

    # main route
    @app.route('/get_characteristics', methods=['POST'])
    def get_characteristics():
        request_data = request.get_json()
        print(request.get_json)
        street = request_data['street']
        city = request_data['city']
        state = request_data['state']
        zip = request_data['zip']

        climate_data = db.get_climate_data(zip)
        google_api_key = app.config['GOOGLE_API_KEY']
        topo_api_key = app.config['OPEN_TOPO_API_KEY']
        topo_data = getTopoData.get_topo_variability(
            street, city, state, zip, google_api_key, topo_api_key)
        population_data = db.get_population_data(zip)

        return jsonify({
            'street': street,
            'city': city,
            'state': state,
            'zip': zip,
            'climate_data': climate_data,
            'topo_data': {'elevation_sd': topo_data},
            'population_data': population_data
        })

    return app
